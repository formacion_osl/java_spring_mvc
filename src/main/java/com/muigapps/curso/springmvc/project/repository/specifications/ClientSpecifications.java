package com.muigapps.curso.springmvc.project.repository.specifications;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.muigapps.curso.springmvc.project.controller.model.FilterClient;
import com.muigapps.curso.springmvc.project.model.Client;
import com.muigapps.curso.springmvc.project.model.OrderClient;
import com.muigapps.curso.springmvc.project.repository.specifications.utils.UtilsSpecifications;

public class ClientSpecifications implements Specification<Client> {


	private static final long serialVersionUID = -1119531885850044166L;
	
	private FilterClient filter;

	public static ClientSpecifications getIntsance(FilterClient filter) {
		return new ClientSpecifications(filter);
	}

	public ClientSpecifications(FilterClient filter) {
		super();
		this.filter = filter;
	}

	@Override
	public Predicate toPredicate(Root<Client> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		List<Predicate> predicates = new ArrayList<Predicate>();
		

		predicates.add(criteriaBuilder.isNull(root.get("deleteDate")));
		
		if (filter != null) {
			if (filter.getEmail() != null  && !filter.getEmail().isEmpty()) {
				predicates.add(UtilsSpecifications.createLike(criteriaBuilder, root, "email", filter.getEmail()));
			}
			
			if (filter.getName() != null  && !filter.getName().isEmpty()) {
				predicates.add(UtilsSpecifications.createLike(criteriaBuilder, root, "name", filter.getName()));
			}
			if (filter.getPhone() != null  && !filter.getPhone().isEmpty()) {
				predicates.add(UtilsSpecifications.createLike(criteriaBuilder, root, "phone", filter.getPhone()));
			}
		}
		
		if (predicates != null && !predicates.isEmpty()) {
			Predicate[] predicatesArray = new Predicate[predicates.size()];
			int pos = 0;
			for (Predicate predicate : predicates) {
				predicatesArray[pos++] = predicate;
			}
			query.distinct(true);
			return criteriaBuilder.and(predicatesArray);
		} else {
			return null;
		}

	}

}
