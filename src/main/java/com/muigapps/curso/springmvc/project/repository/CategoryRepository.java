package com.muigapps.curso.springmvc.project.repository;

import com.muigapps.curso.springmvc.project.model.Category;

public interface CategoryRepository extends BaseEntityRepository<Category, Long> {


}
