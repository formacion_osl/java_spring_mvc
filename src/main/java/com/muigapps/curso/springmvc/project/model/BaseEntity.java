package com.muigapps.curso.springmvc.project.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@MappedSuperclass
public abstract class BaseEntity {

	@JsonIgnore
	@Column(name = "CREATE_USER")
	private String createUser = "default";
	@JsonIgnore
	@Column(name = "UPDATE_USER")
	private String updateUser = "default";
	@JsonIgnore
	@Column(name = "DELETE_USER")
	private String deleteUser;
	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate = new Date();
	@JsonIgnore
	@Column(name = "UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate = new Date();
	@JsonIgnore
	@Column(name = "DELETE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deleteDate;

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getDeleteUser() {
		return deleteUser;
	}

	public void setDeleteUser(String deleteUser) {
		this.deleteUser = deleteUser;
	}

	public Date getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(Date deleteDate) {
		this.deleteDate = deleteDate;
	}

	@Override
	public int hashCode() {
		return Objects.hash(createDate, createUser, deleteDate, deleteUser, updateDate, updateUser);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseEntity other = (BaseEntity) obj;
		return Objects.equals(createDate, other.createDate) && Objects.equals(createUser, other.createUser)
				&& Objects.equals(deleteDate, other.deleteDate) && Objects.equals(deleteUser, other.deleteUser)
				&& Objects.equals(updateDate, other.updateDate) && Objects.equals(updateUser, other.updateUser);
	}

}
