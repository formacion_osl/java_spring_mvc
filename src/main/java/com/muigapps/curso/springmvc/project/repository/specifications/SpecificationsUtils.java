package com.muigapps.curso.springmvc.project.repository.specifications;

import java.text.Normalizer;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;

public class SpecificationsUtils {
	
	public static Expression<String> getExpressionLowerFiled(Path path, CriteriaBuilder criteriaBuilder){
		return criteriaBuilder.lower(criteriaBuilder.function("unaccent", String.class,path));
	}
	
	public static String prepareSearchString(String field) {
		return field != null ? "%"+unaccent(field.toLowerCase())+"%":null;
	}
	
	public static String unaccent(String src) {
		if(src != null) {
			return Normalizer
					.normalize(src, Normalizer.Form.NFD)
					.replaceAll("[^\\p{ASCII}]", "");
		} else {
			return null;
		}
	}

}
