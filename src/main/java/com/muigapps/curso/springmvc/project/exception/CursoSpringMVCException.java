package com.muigapps.curso.springmvc.project.exception;

public class CursoSpringMVCException extends Exception {
	
	private static final long serialVersionUID = -4456383891133818355L;
	private int codeResponse;
	private String messageError;
	
	
	public int getCodeResponse() {
		return codeResponse;
	}


	public void setCodeResponse(int codeResponse) {
		this.codeResponse = codeResponse;
	}


	public String getMessageError() {
		return messageError;
	}


	public void setMessageError(String messageError) {
		this.messageError = messageError;
	}



	public CursoSpringMVCException(int codeResponse, String messageError, Throwable throwable) {
		super(throwable);
		this.codeResponse = codeResponse;
		this.messageError = messageError;
	}
	
	

}
