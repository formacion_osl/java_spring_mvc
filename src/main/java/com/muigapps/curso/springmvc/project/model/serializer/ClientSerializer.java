package com.muigapps.curso.springmvc.project.model.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.muigapps.curso.springmvc.project.model.Client;


public class ClientSerializer extends StdSerializer<Client>{

	protected ClientSerializer() {
		this(null);
		
	}

	protected ClientSerializer(Class<Client> t) {
		super(t);
		
	}

	private static final long serialVersionUID = 1L;

	@Override
	public void serialize(Client value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeStartObject();
		gen.writeNumberField("id", value.getId());
		gen.writeStringField("email", value.getEmail() != null? value.getEmail():"");
		gen.writeStringField("name", value.getName() != null? value.getName():"");
        gen.writeEndObject();
		
	}

}

